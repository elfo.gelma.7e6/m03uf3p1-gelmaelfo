﻿/*
 * Author: Elfo Gelmà
 * Date: 21/02/2023
 * Description: Te muestra un menú en el idioma que hayas elegido al inicio.
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
namespace ChooseLang
{
    class ChooseLang
    {
        static void Main(string[] args)
        {
            string directory = @"F:\Elfo Gelmà\Programació\M3UF3P1-GelmaElfo\Ficheros\";
            MostrarIdioma(ElegirIdioma(directory), directory);
        }

        static int ElegirIdioma(string directory)
        {
            int idioma;
            do
            {
                Console.WriteLine("1-English\n2-Español\n3-Català\n");
            } while (!int.TryParse(Console.ReadLine(), out idioma));
            return idioma;
        }

        static void MostrarIdioma(int idioma, string directory)
        {
            string fichero = directory + @"Files\lang";
            if (Directory.Exists(fichero))
            {
                StreamReader sr;
                string text;
                string[] lines;
                int opcion;
                switch (idioma)
                {
                    case 1:
                        fichero = fichero + "\\en.txt";
                        sr = File.OpenText(fichero);
                        text = sr.ReadToEnd();
                        lines = text.Split("\n");
                        
                        do
                        {
                            Console.Clear();
                            Console.WriteLine(lines[0]);
                            Console.WriteLine(lines[1]);
                            Console.WriteLine(lines[2]);
                        } while (!int.TryParse(Console.ReadLine(), out opcion));
                        if (opcion == 1)
                        {
                            Console.WriteLine(lines[3]);
                        }
                        else if (opcion == 2)
                        {
                            Console.WriteLine(lines[4]);
                        }
                        break;
                    case 2:
                        fichero = fichero + "\\es.txt";
                        sr = File.OpenText(fichero);
                        text = sr.ReadToEnd();
                        lines = text.Split("\n");

                        do
                        {
                            Console.Clear();
                            Console.WriteLine(lines[0]);
                            Console.WriteLine(lines[1]);
                            Console.WriteLine(lines[2]);
                        } while (!int.TryParse(Console.ReadLine(), out opcion));
                        if (opcion == 1)
                        {
                            Console.WriteLine(lines[3]);
                        }
                        else if (opcion == 2)
                        {
                            Console.WriteLine(lines[4]);
                        }
                        break;

                    case 3:
                        fichero = fichero + "\\cat.txt";
                        sr = File.OpenText(fichero);
                        text = sr.ReadToEnd();
                        lines = text.Split("\n");

                        do
                        {
                            Console.Clear();
                            Console.WriteLine(lines[0]);
                            Console.WriteLine(lines[1]);
                            Console.WriteLine(lines[2]);
                        } while (!int.TryParse(Console.ReadLine(), out opcion));
                        if (opcion == 1)
                        {
                            Console.WriteLine(lines[3]);
                        }
                        else if (opcion == 2)
                        {
                            Console.WriteLine(lines[4]);
                        }
                        break;
                }
            }
        }

    }


}
