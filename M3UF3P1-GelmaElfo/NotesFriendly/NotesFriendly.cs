﻿/*
 * Author: Elfo Gelmà
 * Date: 21/02/2023
 * Description:Te dice el numero de calificaciones que tiene un archivo en cada sección creando otro archivo aparte llamado "notesHistory.txt"
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace NotesFriendly
{
    class NotesFriendly
    {
        static void Main(string[] args)
        {
            string directory = @"F:\Elfo Gelmà\Programació\M3UF3P1-GelmaElfo\Ficheros\";
            double[] notas = LeerFichero(directory);
            int[] calificaciones = ComprobarNotas(notas);
            CrearHistorial(directory, calificaciones);

        }

        private static string Titulo(string directory)
        {
            string file = directory + "itb_title.txt";
            if (File.Exists(file))
            {
                StreamReader sr = File.OpenText(file);
                string title = sr.ReadToEnd();
                sr.Close();
                return title;
            }
            else return "El fichero no existe";
            
        }

        private static void CrearHistorial(string directory, int[] califiaciones)
        {
            StreamWriter fichero;
            string ficheroNombre = directory + "notesHistory.txt";
            if (!File.Exists(ficheroNombre)){
                fichero = new StreamWriter(ficheroNombre);
            }
            else
            {
                fichero = File.AppendText(ficheroNombre);
            }
            fichero.WriteLine(Titulo(directory));
            fichero.WriteLine("---------------------\nHistorico de notas\n-------------------- - \n");
            fichero.WriteLine("SUSPÈS:" + califiaciones[3]);
            fichero.WriteLine("Aprovat:" + califiaciones[2]);
            fichero.WriteLine("Notable:" + califiaciones[1]);
            fichero.WriteLine("Excel·lent:" + califiaciones[0]);
            fichero.Close();


        }
        //Comprueba la cantidad de cada calificacion.
        private static int[] ComprobarNotas(double[] notas)
        {
            int[] calificaciones = { 0, 0, 0, 0 }; // Excelentes, Notables, aprobados y suspendidos
            foreach(double nota in notas)
            {
                if (nota <= 10 && nota >= 9)
                {
                    calificaciones[0]++;
                }
                else if (nota < 9 && nota >= 6.5)
                {
                    calificaciones[1]++;
                }
                else if (nota < 6.5 && nota >= 5)
                {
                    calificaciones[2]++;
                }
                else calificaciones[3]++;
            }
            return calificaciones;
        }
        private static double[] LeerFichero(string directory)
        {
            string file = directory + "notestotales.txt";
            if (File.Exists(file))
            {
                StreamReader sr = File.OpenText(file);
                string texto = sr.ReadToEnd();
                texto = texto.Replace(" ", "");
                string[] valores = texto.Split(",");
                double[] notas = new double[valores.Length];
                for(int i = 0; i< notas.Length; i++)
                {
                    notas[i] = Convert.ToDouble(valores[i]);
                }
                sr.Close();
                return notas;
            }
            return null;
        }
    }
}
