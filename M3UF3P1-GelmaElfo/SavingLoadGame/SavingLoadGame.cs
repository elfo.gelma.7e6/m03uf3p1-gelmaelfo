﻿/*
 * Author: Elfo Gelmà
 * Date: 21/02/2023
 * Description: Gestión de partidas de un juego, donde podremos crear, modificar y eliminar datos guardados.
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
namespace SavingLoadGame
{
    class SavingLoadGame
    {
        static void Main()
        {
            var inici = new SavingLoadGame();
            inici.Inici();
        }

        public void Inici()
        {
            int opcio;
            string partida = "";
            string[] estadisticas = { "0", "0", "0" };
            do
            {
                Console.Clear();
                Menu();
                opcio = InsertarOpcio(ref partida, ref estadisticas);



            } while (opcio != 6);


        }
        //S'imprimeix per pantalla un menú.
        public void Menu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1.  Crear nueva partida");
            Console.WriteLine("2.  Guardar Partida");
            Console.WriteLine("3.  Cargar Partida Guardada");
            Console.WriteLine("4.  Modificar Partida");
            Console.WriteLine("5.  Eliminar Partida Guardada");
            Console.WriteLine("6.  Exit");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public int InsertarOpcio(ref string partida, ref string[] estadisticas)
        {
            int option;
            string directory = @"F:\Elfo Gelmà\Programació\M3UF3P1-GelmaElfo\Ficheros\";
            string[] roles = LeerRoles(directory);
            
            do
            {
                Console.WriteLine("Insereix una opcio: ");

            } while (!int.TryParse(Console.ReadLine(), out option));

            switch (option)
            {
                case 6:
                    break;
                case 1:
                    CrearPartida(directory, roles);
                    Console.ReadLine();

                    break;
                case 2:
                    GuardarPartida(partida, estadisticas);
                    Console.ReadLine();
                    break;
                case 3:
                    partida = CargarPartida(directory);
                    Console.ReadLine();
                    break;
                case 4:
                    estadisticas = ModificarPartida(partida);
                    Console.ReadLine();
                    break;
                case 5:
                    EliminarPartida(directory);
                    Console.ReadLine();
                    break;

            }
            return option;
        }

        public string[] LeerRoles(string directory)
        {
            string fichero = directory + "Game\\game.config.txt";
            if (File.Exists(fichero)){
                StreamReader sr = File.OpenText(fichero);
                string texto = sr.ReadToEnd();
                texto = texto.Substring(texto.LastIndexOf(":") + 1);
                sr.Close();
                return texto.Split(",");
                
                
            }
            return null;
        }

        public void CrearPartida(string directory, string[] roles)
        {
            Console.WriteLine("Introduce el nombre: ");
            string name = Console.ReadLine();
            Console.WriteLine("Introduce el nivel: ");
            string nivel = "0";
            string rol;
            do
            {
                Console.WriteLine("Introduce un rol (pícaro,mago,guardabosque,clérigo): ");
                rol = Console.ReadLine();
            } while (!roles.Contains(rol));

            StreamWriter fichero;
            string ficheroNombre = directory + $"Game\\{name}_savedGame.txt";
            if (!File.Exists(ficheroNombre))
            {
                fichero = new StreamWriter(ficheroNombre);
            }
            else
            {
                fichero = File.AppendText(ficheroNombre);
            }
            fichero.WriteLine($"{name};{nivel};{rol}");
            fichero.Close();
        }

        public string CargarPartida(string directory)
        {
            Console.WriteLine("Introduce el fichero que quieres cargar: ");
            string fichero = Console.ReadLine();
            fichero = directory + "Game\\"+fichero;
            if (File.Exists(fichero))
            {
                return fichero;


            }
            Console.WriteLine("El fichero no existe");
            return null;
        }

        public string[] ModificarPartida(string partida)
        {
            if (File.Exists(partida))
            {
                StreamReader sr = File.OpenText(partida);
                string text = sr.ReadToEnd();
                string[] estadisticas = text.Split(";");
                Console.WriteLine("Canvia el nivel: ");
                estadisticas[1] = Console.ReadLine();
                sr.Close();
                return estadisticas;
            }
            return null;
        }

        public void GuardarPartida(string partida, string[] estadisticas)
        {
            StreamWriter fichero;
            fichero = new StreamWriter(partida);
            fichero.WriteLine($"{estadisticas[0]};{estadisticas[1]};{estadisticas[2]}");
            fichero.Close();
        }

        public void EliminarPartida(string directory)
        {
            Console.WriteLine("Introduce la partida que quieres borrar: ");
            string fichero = Console.ReadLine();
            fichero = directory + "Game\\" + fichero;
            if (File.Exists(fichero))
            {
                File.Delete(fichero);
            }
            else Console.WriteLine("No existe un fichero con ese nombre");
        }
    }
}
